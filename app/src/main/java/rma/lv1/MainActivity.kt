package rma.lv1

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import rma.lv1.ui.theme.LV1Theme
import java.security.AllPermission

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LV1Theme {
                // A surface container using the 'background' color from the theme

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background

                ) {
                    BackgroundImage(modifier = Modifier)
                }
            }
        }
    }
}

@Composable
fun UserPreview(name: String, modifier: Modifier = Modifier) {
    val db = FirebaseFirestore.getInstance()
    var visina by remember { mutableStateOf(0f) }
    var tezina by remember { mutableStateOf(0f) }
    var bmi by remember { mutableStateOf(0f) }
    var newTezina by remember { mutableStateOf("") }
    var newVisina by remember { mutableStateOf("") }

    val formattedBmi = String.format("%.2f", bmi)

    fun fetchData() {
        db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW")
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    visina = document.getDouble("Visina")?.toFloat() ?: 0f
                    tezina = document.getDouble("Tezina")?.toFloat() ?: 0f
                    bmi = tezina / (visina * visina)
                }
            }
            .addOnFailureListener { e ->
                Log.e("MainActivity", "Error fetching document: $e")
            }
    }

    LaunchedEffect(Unit) {
        fetchData()
    }

    Text(
        text = "Pozdrav $name!",
        fontSize = 20.sp,
        lineHeight = 56.sp,
        modifier = Modifier
            .padding(top = 8.dp)
            .padding(start = 10.dp)
    )

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Text(
            text = "Tvoj BMI je:",
            fontSize = 55.sp,
            lineHeight = 61.sp,
            textAlign = TextAlign.Center,
        )
        Text(
            text = formattedBmi,
            fontSize = 70.sp,
            lineHeight = 72.sp,
            fontWeight = FontWeight.Bold,
        )
        TextField(
            value = newVisina,
            onValueChange = { newVisina = it },
            label = { Text("Nova Visina:") },
            modifier = Modifier.fillMaxWidth(0.8f)
        )
        Button(onClick = {
            val visinaValue = newVisina.toFloatOrNull()
            if (visinaValue != null) {
                db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW") // Replace with your document ID
                    .update("Visina", visinaValue)
                    .addOnSuccessListener {
                        fetchData()
                    }
                    .addOnFailureListener { e ->
                        Log.e("MainActivity", "Error updating Visina: $e")
                    }
            }
        }) {
            Text("Unesi Visinu")
        }
        TextField(
            value = newTezina,
            onValueChange = { newTezina = it },
            label = { Text("Nova Tezina:") },
            modifier = Modifier.fillMaxWidth(0.8f)
        )
        Button(onClick = {
            val tezinaValue = newTezina.toFloatOrNull()
            if (tezinaValue != null) {
                db.collection("Bmi").document("bTvrdNKpaosSOaEu5GSW") // Replace with your document ID
                    .update("Tezina", tezinaValue)
                    .addOnSuccessListener {
                        fetchData()
                    }
                    .addOnFailureListener { e ->
                        Log.e("MainActivity", "Error updating Tezina: $e")
                    }
            }
        }) {
            Text("Unesi Tezinu")
        }
    }
}


@Composable
fun BackgroundImage(modifier: Modifier) {

    Box (modifier){ Image(
        painter = painterResource(id = R.drawable.fitness),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        alpha = 0.1F
    )
        UserPreview(name = "Mihael", modifier = Modifier.fillMaxSize())
    }

}
@Preview(showBackground = false)
@Composable
fun UserPreview() {
    LV1Theme {
        BackgroundImage(modifier = Modifier)   }
}